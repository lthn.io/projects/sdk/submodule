# Submodules

Project copy of all gitmodule's for stability.

- [randomx](https://gitlab.com/lthn.io/projects/sdk/submodule/-/tree/randomx)
- [trezor-common](https://gitlab.com/lthn.io/projects/sdk/submodule/-/tree/trezor-common)
- [quirc](https://gitlab.com/lthn.io/projects/sdk/submodule/-/tree/quirc)
- [unbound](https://gitlab.com/lthn.io/projects/sdk/submodule/-/tree/unbound)
- [miniupnp](https://gitlab.com/lthn.io/projects/sdk/submodule/-/tree/miniupnp)
- [rapidjson](https://gitlab.com/lthn.io/projects/sdk/submodule/-/tree/rapidjson)